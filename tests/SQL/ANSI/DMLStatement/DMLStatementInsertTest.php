<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementInsert;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWithDuplicateColumnsNameException;

class DMLStatementInsertTest extends TestCase
{
    const TABLE_NAME = 'tableName';
    const LIST_OF_COLUMNS_WITH_DUPLICATE_COLUMNS = ['Field01', 'Field02', 'Field01'];
    const LIST_OF_COLUMNS_WITHOUT_DUPLICATE_COLUMNS = ['Field01', 'Field02', 'Field03'];
    const VALID_INSERT_COMMAND = 'INSERT INTO tableName (Field01, Field02, Field03) VALUES (:Field01, :Field02, :Field03)';

    protected $DMLStatementInsert;

    public function setUp(): void
    {
        parent::setUp();
        $this->DMLStatementInsert = new DMLStatementInsert();
    }

    public function testInstanceOfDMLStatement(): void
    {
        $this->assertInstanceOf(DMLStatementInsert::class, $this->DMLStatementInsert);
    }

    public function testGenerateInsertCommandTableNameIsNotDefined(): void
    {
        $this->expectException(TableNameIsNotDefinedException::class);
        $generatedInsertCommand = $this->DMLStatementInsert->generateInsertCommand('', []);
    }

    public function testGenerateInsertCommandColumnsNameIsNotDefined(): void
    {
        $this->expectException(ColumnsNameIsNotDefinedException::class);
        $generatedInsertCommand = $this->DMLStatementInsert->generateInsertCommand(self::TABLE_NAME, []);
    }

    public function testGenerateInsertCommandWithDuplicateColumnsName(): void
    {
        $this->expectException(ColumnsNameWithDuplicateColumnsNameException::class);
        $generatedInsertCommand = $this->DMLStatementInsert->generateInsertCommand(self::TABLE_NAME, self::LIST_OF_COLUMNS_WITH_DUPLICATE_COLUMNS);
    }

    public function testGenerateInsertCommandValidCommand(): void
    {
        $generatedInsertCommand = $this->DMLStatementInsert->generateInsertCommand(self::TABLE_NAME, self::LIST_OF_COLUMNS_WITHOUT_DUPLICATE_COLUMNS);
        $this->assertEquals(self::VALID_INSERT_COMMAND, $generatedInsertCommand);
    }
}
