<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementUpdate;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWithDuplicateColumnsNameException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseWithDuplicateColumnsNameException;

class DMLStatementUpdateTest extends TestCase
{
    const TABLE_NAME = 'tableName';
    const COLUMNS_WITH_DUPLICATE_COLUMNS = ['Field01', 'Field02', 'Field01'];
    const COLUMNS_WITHOUT_DUPLICATE_COLUMNS = ['Field01', 'Field02', 'Field03'];
    const COLUMNS_WHERE_CLAUSE_WITH_DUPLICATE_COLUMNS = ['Field01', 'Field01'];
    const COLUMNS_WHERE_CLAUSE_WITH_ONE_COLUMN = ['Field01'];
    const COLUMNS_WHERE_CLAUSE_WITH_TWO_COLUMN = ['Field01', 'Field02'];

    const VALID_UPDATE_COMMAND_WITH_ONE_COLUMN_WHERE_CLAUSE = 'UPDATE tableName SET Field01 = :Field01, Field02 = :Field02, Field03 = :Field03 WHERE (Field01 = :Field01)';
    const VALID_UPDATE_COMMAND_WITH_TWO_COLUMN_WHERE_CLAUSE = 'UPDATE tableName SET Field01 = :Field01, Field02 = :Field02, Field03 = :Field03 WHERE (Field01 = :Field01 AND Field02 = :Field02)';

    protected $DMLStatementUpdate;

    public function setUp(): void
    {
        parent::setUp();
        $this->DMLStatementUpdate = new DMLStatementUpdate();
    }

    public function testInstanceOfDMLStatement(): void
    {
        $this->assertInstanceOf(DMLStatementUpdate::class, $this->DMLStatementUpdate);
    }

    public function testGenerateUpdateCommandTableNameIsNotDefined(): void
    {
        $this->expectException(TableNameIsNotDefinedException::class);
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand('', [], []);
    }

    public function testGenerateUpdateCommandColumnsNameIsNotDefined(): void
    {
        $this->expectException(ColumnsNameIsNotDefinedException::class);
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME, [], []);
    }

    public function testGenerateUpdateCommandWithDuplicateColumnsName(): void
    {
        $this->expectException(ColumnsNameWithDuplicateColumnsNameException::class);
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME, self::COLUMNS_WITH_DUPLICATE_COLUMNS, []);
    }

    public function testGenerateUpdateCommandColumnsNameWhereClauseIsNotDefined(): void
    {
        $this->expectException(ColumnsNameWhereClauseIsNotDefinedException::class);
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME, self::COLUMNS_WITHOUT_DUPLICATE_COLUMNS, []);
    }

    public function testGenerateUpdateCommandColumnsNameWhereClauseWithDuplicateColumnsName(): void
    {
        $this->expectException(ColumnsNameWhereClauseWithDuplicateColumnsNameException::class);
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME,
            self::COLUMNS_WITHOUT_DUPLICATE_COLUMNS,
            self::COLUMNS_WHERE_CLAUSE_WITH_DUPLICATE_COLUMNS);
    }

    public function testGenerateUpdateCommandValidCommandWithOneColumnWhereClause(): void
    {
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME,
            self::COLUMNS_WITHOUT_DUPLICATE_COLUMNS,
            self::COLUMNS_WHERE_CLAUSE_WITH_ONE_COLUMN);
        $this->assertEquals(self::VALID_UPDATE_COMMAND_WITH_ONE_COLUMN_WHERE_CLAUSE, $generatedUpdateCommand);
    }

    public function testGenerateUpdateCommandValidCommandWithTwoColumnWhereClause(): void
    {
        $generatedUpdateCommand = $this->DMLStatementUpdate->generateUpdateCommand(self::TABLE_NAME,
            self::COLUMNS_WITHOUT_DUPLICATE_COLUMNS,
            self::COLUMNS_WHERE_CLAUSE_WITH_TWO_COLUMN);
        $this->assertEquals(self::VALID_UPDATE_COMMAND_WITH_TWO_COLUMN_WHERE_CLAUSE, $generatedUpdateCommand);
    }
}
