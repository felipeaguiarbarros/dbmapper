<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementDelete;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseWithDuplicateColumnsNameException;

class DMLStatementDeleteTest extends TestCase
{
    const TABLE_NAME = 'tableName';
    const COLUMNS_WHERE_CLAUSE_WITH_DUPLICATE_COLUMNS = ['Field01', 'Field01'];
    const COLUMNS_WHERE_CLAUSE_WITH_ONE_COLUMN = ['Field01'];
    const COLUMNS_WHERE_CLAUSE_WITH_TWO_COLUMN = ['Field01', 'Field02'];

    const VALID_DELETE_COMMAND_WITH_ONE_COLUMN_WHERE_CLAUSE = 'DELETE FROM tableName WHERE (Field01 = :?Field01?)';
    const VALID_DELETE_COMMAND_WITH_TWO_COLUMN_WHERE_CLAUSE = 'DELETE FROM tableName WHERE (Field01 = :?Field01? AND Field02 = :?Field02?)';

    protected $DMLStatementDelete;

    public function setUp(): void
    {
        parent::setUp();
        $this->DMLStatementDelete = new DMLStatementDelete();
    }

    public function testInstanceOfDMLStatement(): void
    {
        $this->assertInstanceOf(DMLStatementDelete::class, $this->DMLStatementDelete);
    }
    
    public function testGenerateDeleteCommandTableNameIsNotDefined(): void
    {
        $this->expectException(TableNameIsNotDefinedException::class);
        $generatedDeleteCommand = $this->DMLStatementDelete->generateDeleteCommand('', []);
    }

    public function testGenerateDeleteCommandColumnsNameWhereClauseIsNotDefined(): void
    {
        $this->expectException(ColumnsNameWhereClauseIsNotDefinedException::class);
        $generatedDeleteCommand = $this->DMLStatementDelete->generateDeleteCommand(self::TABLE_NAME, []);
    }

    public function testGenerateDeleteCommandColumnsNameWhereClauseWithDuplicateColumnsName(): void
    {
        $this->expectException(ColumnsNameWhereClauseWithDuplicateColumnsNameException::class);
        $generatedDeleteCommand = $this->DMLStatementDelete->generateDeleteCommand(self::TABLE_NAME,
            self::COLUMNS_WHERE_CLAUSE_WITH_DUPLICATE_COLUMNS);
    }

    public function testGenerateDeleteCommandValidCommandWithOneColumnWhereClause(): void
    {
        $generatedDeleteCommand = $this->DMLStatementDelete->generateDeleteCommand(self::TABLE_NAME,
            self::COLUMNS_WHERE_CLAUSE_WITH_ONE_COLUMN);
        $this->assertEquals(self::VALID_DELETE_COMMAND_WITH_ONE_COLUMN_WHERE_CLAUSE, $generatedDeleteCommand);
    }

    public function testGenerateDeleteCommandValidCommandWithTwoColumnWhereClause(): void
    {
        $generatedDeleteCommand = $this->DMLStatementDelete->generateDeleteCommand(self::TABLE_NAME,
            self::COLUMNS_WHERE_CLAUSE_WITH_TWO_COLUMN);
        $this->assertEquals(self::VALID_DELETE_COMMAND_WITH_TWO_COLUMN_WHERE_CLAUSE, $generatedDeleteCommand);
    }
}
