<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementSequence;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\SequenceNameIsNotDefinedException;

class DMLStatementSequenceTest extends TestCase
{
    const SEQUENCE_NAME = 'sequenceName';
    const VALID_NEXT_VAL_COMMAND = "SELECT NEXTVAL('sequenceName')";

    protected $DMLStatementSequence;

    public function setUp(): void
    {
        parent::setUp();
        $this->DMLStatementSequence = new DMLStatementSequence();
    }

    public function testInstanceOfDMLStatement(): void
    {
        $this->assertInstanceOf(DMLStatementSequence::class, $this->DMLStatementSequence);
    }

    public function testGenerateNextValCommandSequenceNameIsNotDefined(): void
    {
        $this->expectException(SequenceNameIsNotDefinedException::class);
        $generatedNextValCommand = $this->DMLStatementSequence->generateNextValCommand('');
    }

    public function testGenerateNextValCommandValidCommand(): void
    {
        $generatedNextValCommand = $this->DMLStatementSequence->generateNextValCommand(self::SEQUENCE_NAME);
        $this->assertEquals(self::VALID_NEXT_VAL_COMMAND,
            $generatedNextValCommand);
    }
}