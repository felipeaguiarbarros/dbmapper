<?php

namespace Nwz\DBMapper\Tests\DummyClasses;

use Nwz\DBMapper\Mapper\Mapper;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategy;

class DummyACountryMapper extends Mapper
{
    public function __construct()
    {
        $this->setMappedClass(DummyACountry::class);

        $this->setTableName('ACountry');

        $this->setColumn('aid', 'aid');
        $this->setColumn('aname');
        $this->setColumn('aalpha2Code', 'aalpha2Code');

        $this->setPrimaryKey('aid', new PrimaryKeyStrategy('sequence', 'SqACountryId'));
    }
}
