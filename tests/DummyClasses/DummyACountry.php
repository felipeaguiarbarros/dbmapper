<?php

namespace Nwz\DBMapper\Tests\DummyClasses;

class DummyACountry
{
    private $aid;
    private $aname;
    private $anumericCode;
    private $aalpha2Code;
    private $aalpha3Code;
    
    public function __construct($aid = '', $aname = '', $anumericCode = '', $aalpha2Code = '', $aalpha3Code = '')
    {
        $this->aid = $aid;
        $this->aname = $aname;
        $this->anumericCode = $anumericCode;
        $this->aalpha2Code = $aalpha2Code;
        $this->aalpha3Code = $aalpha3Code;
    }
}
