<?php

namespace Nwz\DBMapper\Tests\DummyClasses;

use Nwz\DBMapper\DBContext\DBContext;

class DummyDBContext extends DBContext
{
    public function __construct()
    {
        $this->addMapper(new DummyCountryMapper());
        $this->addMapper(new DummyACountryMapper());
    }
}
