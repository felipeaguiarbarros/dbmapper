<?php

namespace Nwz\DBMapper\Tests\DummyClasses;

class DummyCountry
{
    private $id;
    private $name;
    private $numericCode;
    private $alpha2Code;
    private $alpha3Code;
    
    public function __construct($id = '', $name = '', $numericCode = '', $alpha2Code = '', $alpha3Code = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->numericCode = $numericCode;
        $this->alpha2Code = $alpha2Code;
        $this->alpha3Code = $alpha3Code;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setAlpha3Code(string $alpha3Code)
    {
        $this->alpha3Code = $alpha3Code;
    }
}
