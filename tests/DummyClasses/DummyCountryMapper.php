<?php

namespace Nwz\DBMapper\Tests\DummyClasses;

use Nwz\DBMapper\Mapper\Mapper;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategy;

class DummyCountryMapper extends Mapper
{
    public function __construct()
    {
        $this->setMappedClass(DummyCountry::class);

        $this->setTableName('Country');

        $this->setColumn('id', 'id');
        $this->setColumn('name');
        $this->setColumn('alpha2Code', 'alpha2Code');

        $this->setPrimaryKey('id', new PrimaryKeyStrategy('sequence', 'SqCountryId'));
    }
}
