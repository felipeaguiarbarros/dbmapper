testMapperHasKeyDummyCountryClass<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\DBContext\Exceptions\DatabaseConnectionStringIsNotSettingException;

use Nwz\DBMapper\Tests\DummyClasses\DummyCountry;
use Nwz\DBMapper\Tests\DummyClasses\DummyCountryMapper;
use Nwz\DBMapper\Tests\DummyClasses\DummyACountry;
use Nwz\DBMapper\Tests\DummyClasses\DummyACountryMapper;

class DBContextTest extends TestCase
{
    private $dummyDBContext;
    private $dummyCountry;
    private $dummyCountry0;
    private $dummyCountry1;
    private $dummyACountry;

    public function setUp(): void
    {
        parent::setUp();
        $this->dummyDBContext = $this->getMockForAbstractClass('Nwz\DBMapper\Tests\DummyClasses\DummyDBContext');
        $this->dummyCountry = new DummyCountry('1', 'DUMMY COUNTRY', '000', '00', '000');
        $this->dummyCountry0 = new DummyCountry('1', 'DUMMY COUNTRY', '000', '00', '000');
        $this->dummyCountry1 = new DummyCountry('1', 'DUMMY COUNTRY', '000', '00', '000');
        $this->dummyACountry = new DummyACountry('1', 'DUMMY COUNTRY', '000', '00', '000');
    }

    public function testSetAndGetDatabaseConnectionString(): void
    {
        $this->dummyDBContext->setDatabaseConnectionString('pgsql:host=127.0.0.1;port=5432;dbname=zero;user=postgres;password=0');
        $this->assertEquals('pgsql:host=127.0.0.1;port=5432;dbname=zero;user=postgres;password=0', $this->dummyDBContext->getDatabaseConnectionString());
    }

    public function testConnectDatabaseDatabaseConnectionStringIsNotSettingException(): void
    {
        $this->expectException(DatabaseConnectionStringIsNotSettingException::class);
        $this->dummyDBContext->connectDatabase();
    }

    public function testConnectDatabase(): void
    {
        $this->dummyDBContext->setDatabaseConnectionString('pgsql:host=127.0.0.1;port=5432;dbname=zero;user=postgres;password=0');
        $this->assertEquals(true, $this->dummyDBContext->connectDatabase());
    }

    public function testMapperHasKeyDummyCountryClass(): void
    {
        $this->assertArrayHasKey(DummyCountry::class, $this->dummyDBContext->getMappers());
    }

    public function testMapperKeyDummyCountryClassIsInstanceOfDummyCountryMapper(): void
    {
        $this->assertInstanceOf(DummyCountryMapper::class, $this->dummyDBContext->getMappers()[DummyCountry::class]);
    }

    public function testAdd(): void
    {
        $objectHash = spl_object_hash($this->dummyCountry);
        $this->dummyDBContext->add($this->dummyCountry);

        $this->assertArrayHasKey($objectHash, $this->dummyDBContext->getObjectsMappedClass());
        $this->assertArrayHasKey($objectHash, $this->dummyDBContext->getObjectsInserts());

        $this->assertEquals('INSERT INTO Country (id, name, alpha2Code) VALUES (:id, :name, :alpha2Code)',
            $this->dummyDBContext->getPrepareStatementsInserts()[sha1('INSERT INTO Country (id, name, alpha2Code) VALUES (:id, :name, :alpha2Code)')]);

        $this->assertEquals('SELECT NEXTVAL(\'SqCountryId\')',
            $this->dummyDBContext->getPrepareStatementsInsertsSequences()[sha1('INSERT INTO Country (id, name, alpha2Code) VALUES (:id, :name, :alpha2Code)')]);
    }

    public function testX(): void
    {
        $this->dummyDBContext->setDatabaseConnectionString('pgsql:host=127.0.0.1;port=5432;dbname=zero;user=postgres;password=0');

        $this->dummyDBContext->add($this->dummyCountry);
        $this->dummyDBContext->add($this->dummyACountry);
        $this->dummyDBContext->add($this->dummyCountry0);
        $this->dummyDBContext->add($this->dummyCountry1);
        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->dummyCountry->setName('DUMMY COUNTRY CHANGED');
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
        $this->dummyCountry0->setName('DUMMY COUNTRY CHANGED2');
        $this->dummyCountry0->setAlpha3Code('DUMMY COUNTRY CHANGED2');
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->dummyDBContext->add($this->dummyCountry0);
//        $this->dummyDBContext->add($this->dummyCountry0);
//        $this->dummyDBContext->add($this->dummyCountry0);
//        $this->dummyDBContext->add($this->dummyCountry0);
//        $this->dummyDBContext->add($this->dummyCountry0);
        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->dummyDBContext->add($this->dummyCountry0);
        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
//        $this->assertEquals(true, $this->dummyDBContext->saveChanges());
    }
}
