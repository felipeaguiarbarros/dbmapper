<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategy;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategyEmptyStrategyTypeException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategyInvalidTypeException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategySequenceNameMustNotBeInformedException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategySequenceNameMustBeInformedException;

class PrimaryKeyStrategyTest extends TestCase
{
    const PRIMARY_KEY_STRATEGY_INVALID_TYPE = 'anyValue';
    const PRIMARY_KEY_STRATEGY_NONE_TYPE = 'none';
    const PRIMARY_KEY_STRATEGY_SEQUENCE_TYPE = 'sequence';
    const PRIMARY_KEY_STRATEGY_SEQUENCE_NAME = 'sequenceName';

    public function testInstanceOfPrimaryKeyStrategy(): void
    {
        $this->assertInstanceOf(PrimaryKeyStrategy::class, new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_NONE_TYPE));
    }

    public function testPrimaryKeyStrategyEmptyStrategyTypeException(): void
    {
        $this->expectException(PrimaryKeyStrategyEmptyStrategyTypeException::class);
        $primaryKeyStrategy = new PrimaryKeyStrategy('');
    }

    public function testPrimaryKeyStrategyInvalidTypeException(): void
    {
        $this->expectException(PrimaryKeyStrategyInvalidTypeException::class);
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_INVALID_TYPE);
    }

    public function testPrimaryKeyStrategySequenceNameMustNotBeInformedException(): void
    {
        $this->expectException(PrimaryKeyStrategySequenceNameMustNotBeInformedException::class);
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_NONE_TYPE,
            self::PRIMARY_KEY_STRATEGY_SEQUENCE_NAME);
    }

    public function testPrimaryKeyStrategySequenceNameMustBeInformedException(): void
    {
        $this->expectException(PrimaryKeyStrategySequenceNameMustBeInformedException::class);
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_SEQUENCE_TYPE,'');
    }

    public function testPrimaryKeyStrategyNoneType(): void
    {
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_NONE_TYPE);
        $this->assertEquals(self::PRIMARY_KEY_STRATEGY_NONE_TYPE, $primaryKeyStrategy->getPrimaryKeyStrategyType());
    }

    public function testPrimaryKeyStrategySequenceType(): void
    {
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_SEQUENCE_TYPE, self::PRIMARY_KEY_STRATEGY_SEQUENCE_NAME);
        $this->assertEquals(self::PRIMARY_KEY_STRATEGY_SEQUENCE_TYPE, $primaryKeyStrategy->getPrimaryKeyStrategyType());
    }

    public function testPrimaryKeyStrategyGetSequenceName(): void
    {
        $primaryKeyStrategy = new PrimaryKeyStrategy(self::PRIMARY_KEY_STRATEGY_SEQUENCE_TYPE, self::PRIMARY_KEY_STRATEGY_SEQUENCE_NAME);
        $this->assertEquals(self::PRIMARY_KEY_STRATEGY_SEQUENCE_NAME, $primaryKeyStrategy->getPrimaryKeySequenceName());
    }
}
