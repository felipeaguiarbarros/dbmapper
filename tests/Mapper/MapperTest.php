<?php

use PHPUnit\Framework\TestCase;

use Nwz\DBMapper\Mapper\Exception\MappedClassIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\TableHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\PropertyNameIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\PropertyHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\ColumnHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\PropertyHasNotAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategy;

class MapperTest extends TestCase
{
    const TABLE_NAME = 'tableName';
    const MAPPED_CLASS_NAME = 'mappedClass';

    private $mapper;

    public function setUp(): void
    {
        parent::setUp();
        $this->mapper = $this->getMockForAbstractClass('Nwz\DBMapper\Mapper\Mapper');
    }

    public function testSetMappedClass(): void
    {
        $this->expectException(MappedClassIsNotDefinedException::class);
        $this->mapper->setMappedClass('');
    }

    public function testGetMappedClass(): void
    {
        $this->mapper->setMappedClass(self::MAPPED_CLASS_NAME);
        $this->assertEquals(self::MAPPED_CLASS_NAME, $this->mapper->getMappedClass());
    }

    public function testSetTableTableNameIsNotDefined(): void
    {
        $this->expectException(TableNameIsNotDefinedException::class);
        $this->mapper->setTableName('');
    }

    public function testSetTableTableHasAlreadyBeenMapped(): void
    {
        $this->expectException(TableHasAlreadyBeenMappedException::class);
        $this->mapper->setTableName(self::TABLE_NAME);
        $this->mapper->setTableName(self::TABLE_NAME);
    }

    public function testGetTableName(): void
    {
        $this->mapper->setTableName(self::TABLE_NAME);
        $this->assertEquals(self::TABLE_NAME, $this->mapper->getTableName());
    }

    public function testSetColumnPropertyNameIsNotDefined(): void
    {
        $this->expectException(PropertyNameIsNotDefinedException::class);
        $this->mapper->setColumn('');
    }

    public function testSetColumnPropertyHasAlreadyBeenMappedException(): void
    {
        $this->expectException(PropertyHasAlreadyBeenMappedException::class);
        $this->mapper->setColumn('propertyName');
        $this->mapper->setColumn('propertyName');
    }

    public function testSetColumnColumnHasAlreadyBeenMappedException(): void
    {
        $this->expectException(ColumnHasAlreadyBeenMappedException::class);
        $this->mapper->setColumn('propertyName0', 'columnName');
        $this->mapper->setColumn('propertyName1', 'columnName');
    }

    public function testGetPropertiesNameIndexedByColumnName(): void
    {
        $this->mapper->setColumn('propertyName', 'columnName');

        $propertiesNameIndexedByColumnName = [];
        $propertiesNameIndexedByColumnName = $this->mapper->getPropertiesNameIndexedByColumnName();

        $this->assertIsArray($propertiesNameIndexedByColumnName);
        $this->assertNotEmpty($propertiesNameIndexedByColumnName);
        $this->assertArrayHasKey('columnName', $propertiesNameIndexedByColumnName);
    }

    public function testGetColumnsNameIndexedByPropertyName(): void
    {
        $this->mapper->setColumn('propertyName', 'columnName');

        $columnsNameIndexedByPropertyName = [];
        $columnsNameIndexedByPropertyName = $this->mapper->getColumnsNameIndexedByPropertyName();

        $this->assertIsArray($columnsNameIndexedByPropertyName);
        $this->assertNotEmpty($columnsNameIndexedByPropertyName);
        $this->assertArrayHasKey('propertyName', $columnsNameIndexedByPropertyName);
    }

    public function testGetColumnNameByPropertyName(): void
    {
        $this->mapper->setColumn('propertyName0', 'columnName0');
        $this->mapper->setColumn('propertyName1', 'columnName1');

        $this->assertEquals('columnName0', $this->mapper->getColumnNameByPropertyName('propertyName0'));
    }

    public function testSetPrimaryKeyPropertyNameIsNotDefined(): void
    {
        $this->expectException(PropertyNameIsNotDefinedException::class);
        $this->mapper->setPrimaryKey('', new PrimaryKeyStrategy('none'));
    }

    public function testSetPrimaryKeyPropertyHasNotAlreadyBeenMappedException(): void
    {
        $this->expectException(PropertyHasNotAlreadyBeenMappedException::class);
        $this->mapper->setPrimaryKey('propertyName', new PrimaryKeyStrategy('none'));
    }

    public function testGetPrimaryKeyPropertyName(): void
    {
        $this->mapper->setColumn('propertyName');
        $this->mapper->setPrimaryKey('propertyName', new PrimaryKeyStrategy('none'));
        $this->assertEquals('propertyName', $this->mapper->getPrimaryKeyPropertyName());
    }

    public function testGetPrimaryKeyColumnName(): void
    {
        $this->mapper->setColumn('propertyName', 'columnName');
        $this->mapper->setPrimaryKey('propertyName', new PrimaryKeyStrategy('none'));
        $this->assertEquals('columnName', $this->mapper->getPrimaryKeyColumnName());
    }

    public function testGetPrimaryKeyStrategy(): void
    {
        $this->mapper->setColumn('propertyName');
        $this->mapper->setPrimaryKey('propertyName', new PrimaryKeyStrategy('none'));
        $this->assertInstanceOf(PrimaryKeyStrategy::class, $this->mapper->getPrimaryKeyStrategy());
        $this->assertEquals('none', $this->mapper->getPrimaryKeyStrategy()->getPrimaryKeyStrategyType());
    }
}
