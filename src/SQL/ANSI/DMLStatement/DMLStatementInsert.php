<?php

namespace Nwz\DBMapper\SQL\ANSI\DMLStatement;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWithDuplicateColumnsNameException;

class DMLStatementInsert
{
    public function generateInsertCommand(string $tableName, array $columnsName): string
    {
        if (trim($tableName) === '') {
            throw new TableNameIsNotDefinedException();
        }

        if (Count($columnsName) === 0) {
            throw new ColumnsNameIsNotDefinedException();
        }

        if (Count($columnsName) !== Count(array_unique($columnsName))) {
            throw new ColumnsNameWithDuplicateColumnsNameException();
        }

        return 'INSERT INTO ' . $tableName . ' (' . implode(', ', $columnsName) . ')' . ' VALUES (' .  ':' . implode(', :', $columnsName) . ')';
    }
}
