<?php

namespace Nwz\DBMapper\SQL\ANSI\DMLStatement;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseWithDuplicateColumnsNameException;

class DMLStatementDelete
{
    public function generateDeleteCommand(string $tableName, array $whereClauseColumnsName): string
    {
        if (trim($tableName) === '') {
            throw new TableNameIsNotDefinedException();
        }

        if (Count($whereClauseColumnsName) === 0) {
            throw new ColumnsNameWhereClauseIsNotDefinedException();
        }

        if (Count($whereClauseColumnsName) !== Count(array_unique($whereClauseColumnsName))) {
            throw new ColumnsNameWhereClauseWithDuplicateColumnsNameException();
        }

        $updateWhereClause = '';
        $andSeparator = '';
        foreach ($whereClauseColumnsName as $whereClauseColumnName)
        {
            $updateWhereClause .= $andSeparator . $whereClauseColumnName . ' = ' . ':' . '?' . $whereClauseColumnName . '?';
            $andSeparator = ' AND ';
        }

        return 'DELETE FROM ' . $tableName . ' WHERE (' . $updateWhereClause . ')';
    }
}
