<?php

namespace Nwz\DBMapper\SQL\ANSI\DMLStatement;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\SequenceNameIsNotDefinedException;

class DMLStatementSequence
{
    public function generateNextValCommand(string $sequenceName): string
    {
        if (trim($sequenceName) === '') {
            throw new SequenceNameIsNotDefinedException();
        }

        return 'SELECT NEXTVAL(' . '\'' . $sequenceName . '\'' . ')';
    }
}
