<?php

namespace Nwz\DBMapper\SQL\ANSI\DMLStatement;

use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWithDuplicateColumnsNameException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseIsNotDefinedException;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\Exception\ColumnsNameWhereClauseWithDuplicateColumnsNameException;

class DMLStatementUpdate
{
    public function generateUpdateCommand(string $tableName, array $columnsName, array $whereClauseColumnsName): string
    {
        if (trim($tableName) === '') {
            throw new TableNameIsNotDefinedException();
        }

        if (Count($columnsName) === 0) {
            throw new ColumnsNameIsNotDefinedException();
        }

        if (Count($columnsName) !== Count(array_unique($columnsName))) {
            throw new ColumnsNameWithDuplicateColumnsNameException();
        }

        if (Count($whereClauseColumnsName) === 0) {
            throw new ColumnsNameWhereClauseIsNotDefinedException();
        }

        if (Count($whereClauseColumnsName) !== Count(array_unique($whereClauseColumnsName))) {
            throw new ColumnsNameWhereClauseWithDuplicateColumnsNameException();
        }

        $updateSet = '';
        $commaSeparator = '';
        foreach ($columnsName as $columnName)
        {
            $updateSet .= $commaSeparator . $columnName . ' = ' . ':' . $columnName;
            $commaSeparator = ', ';
        }

        $updateWhereClause = '';
        $andSeparator = '';
        foreach ($whereClauseColumnsName as $whereClauseColumnName)
        {
            $updateWhereClause .= $andSeparator . $whereClauseColumnName . ' = ' . ':' . $whereClauseColumnName;
            $andSeparator = ' AND ';
        }

        return 'UPDATE ' . $tableName . ' SET ' . $updateSet . ' WHERE (' . $updateWhereClause . ')';
    }
}
