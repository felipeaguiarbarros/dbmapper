<?php

namespace Nwz\DBMapper\DBContext;

use Nwz\DBMapper\DBContext\Exceptions\DatabaseConnectionStringIsNotSettingException;
use Nwz\DBMapper\LJReflection\LJReflection;
use Nwz\DBMapper\Mapper\MapperInterface;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementInsert;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementSequence;
use Nwz\DBMapper\SQL\ANSI\DMLStatement\DMLStatementUpdate;

abstract class DBContext
{
    private $connectionString;
    private $connection;

    private $mappers = [];
    private $objectsMappedClass = [];
    private $objectsInserts = [];
    private $prepareStatementsInserts = [];
    private $prepareStatementsInsertsSequences = [];
    private $objectHashByPrepareStatementsInserts = [];
    private $objectsOperations = [];

    private $prepareStatementsUpdates = [];
    private $objectHashByPrepareStatementsUpdates = [];

    private $j = [];

    public function setDatabaseConnectionString(string $connectionString)
    {
        $this->connectionString = $connectionString;
    }

    public function getDatabaseConnectionString(): string
    {
        return $this->connectionString;
    }

    public function connectDatabase(): bool
    {
        if (trim($this->connectionString) === '') {
            throw new DatabaseConnectionStringIsNotSettingException();
        }

        if ($this->connection === null) {
            $this->connection = new \PDO($this->connectionString);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_UPPER);
        }

        return true;
    }

    protected function addMapper(MapperInterface $mapper): void
    {
        $this->mappers[$mapper->getMappedClass()] = $mapper;
    }

    public function getMappers(): array
    {
        return $this->mappers;
    }

    public function add(Object $object): void
    {
        $objectHash = spl_object_hash($object);
        $this->objectsMappedClass[$objectHash] = $this->mappers[get_class($object)]->getMappedClass();
        $this->objectsInserts[$objectHash] = $object;

        $this->objectsOperations[$objectHash] = 'i';

        /*
         * Insert
         */

        $DMLStatementInsert = new DMLStatementInsert();
        $generateInsertCommand = $DMLStatementInsert->generateInsertCommand($this->mappers[$this->objectsMappedClass[$objectHash]]->getTableName(),
            $this->mappers[$this->objectsMappedClass[$objectHash]]->getColumnsNameIndexedByPropertyName());

        $generateInsertCommandHash = sha1($generateInsertCommand);
        $this->prepareStatementsInserts[$generateInsertCommandHash] = $generateInsertCommand;
        $this->objectHashByPrepareStatementsInserts[$objectHash] = $generateInsertCommandHash;

        /*
         * Sequence
         */

        $primaryKeyStrategy = $this->mappers[$this->objectsMappedClass[$objectHash]]->getPrimaryKeyStrategy();
        if ($primaryKeyStrategy->getPrimaryKeyStrategyType() === 'sequence') {
            $this->prepareStatementsInsertsSequences[$generateInsertCommandHash] = (new DMLStatementSequence())->generateNextValCommand($primaryKeyStrategy->getPrimaryKeySequenceName());
        }
        else {
            $this->prepareStatementsInsertsSequences[$generateInsertCommandHash] = null;
        }

        /*
         * Update
         */

        $columnsName = $this->mappers[$this->objectsMappedClass[$objectHash]]->getColumnsNameIndexedByPropertyName();
        unset($columnsName[$this->mappers[$this->objectsMappedClass[$objectHash]]->getPrimaryKeyPropertyName()]);

        $DMLStatementUpdate = new DMLStatementUpdate();
        $generateUpdateCommand = $DMLStatementUpdate->generateUpdateCommand($this->mappers[$this->objectsMappedClass[$objectHash]]->getTableName(),
            $columnsName,
            [$this->mappers[$this->objectsMappedClass[$objectHash]]->getPrimaryKeyColumnName()]
        );

        $generateUpdateCommandHash = sha1($generateUpdateCommand);
        $this->prepareStatementsUpdates[$generateUpdateCommandHash] = $generateUpdateCommand;

        $this->objectHashByPrepareStatementsUpdates[$objectHash] = $generateUpdateCommandHash;
    }

    public function getObjectsMappedClass(): array
    {
        return $this->objectsMappedClass;
    }

    public function getObjectsInserts(): array
    {
        return $this->objectsInserts;
    }

    public function getPrepareStatementsInserts(): array
    {
        return $this->prepareStatementsInserts;
    }

    public function getPrepareStatementsInsertsSequences(): array
    {
        return $this->prepareStatementsInsertsSequences;
    }

    private function persistInserts(): void
    {
        $LJReflection = new LJReflection();

        $objectsOperations = array_filter($this->objectsOperations,
            function($objectHash)
            {
                return $objectHash === 'i';
            }
            );

        $currentPrepareStatementsInserts = '';
        foreach ($objectsOperations as $objectOperationKey => $objectOperation) {
            $objectInsert = $this->objectsInserts[$objectOperationKey];

            $primaryKeyPropertyName = $this->mappers[$this->objectsMappedClass[$objectOperationKey]]->getPrimaryKeyPropertyName();

            $LJReflection->setObject($objectInsert);
            $objectInsertByArray = $LJReflection->extractToArray();

            $doBind = array_intersect_ukey($objectInsertByArray,
                $this->mappers[$this->objectsMappedClass[$objectOperationKey]]->getColumnsNameIndexedByPropertyName(),
                function($firstKey, $secondKey)
                {
                    return $firstKey <=> $secondKey;
                }
            );

            if ($currentPrepareStatementsInserts !== $this->objectHashByPrepareStatementsInserts[$objectOperationKey]) {
                $currentPrepareStatementsInserts = $this->objectHashByPrepareStatementsInserts[$objectOperationKey];
                $doPrepareStatementInsert = $this->connection->prepare($this->prepareStatementsInserts[$this->objectHashByPrepareStatementsInserts[$objectOperationKey]]);
            }

            if (!is_null($this->prepareStatementsInsertsSequences[$this->objectHashByPrepareStatementsInserts[$objectOperationKey]])) {
                $doPrepareStatementInsertSequence = $this->connection->prepare($this->prepareStatementsInsertsSequences[$this->objectHashByPrepareStatementsInserts[$objectOperationKey]]);
                $doPrepareStatementInsertSequence->execute();
                $primaryKeyValue = $doPrepareStatementInsertSequence->fetch()[0];
                $doBind[$primaryKeyPropertyName] = $primaryKeyValue;
                $LJReflection->setPropertyValue($primaryKeyPropertyName, $primaryKeyValue);
            }

            foreach($doBind as $doBindKey => $doBindValue) {
                $bindColumnName = $this->mappers[$this->objectsMappedClass[$objectOperationKey]]->getColumnNameByPropertyName($doBindKey);
                    $doPrepareStatementInsert->bindValue($bindColumnName, $doBindValue);
            }

            $doPrepareStatementInsert->execute();

            $this->j[$objectOperationKey] = json_encode($doBind);
        }
    }

    private function persistUpdates(): void
    {
        $LJReflection = new LJReflection();

        $objectsOperations = array_filter($this->objectsOperations,
            function($objectHash)
            {
                return (($objectHash === 'i') || ($objectHash === 'u'));
            }
        );

        foreach ($objectsOperations as $objectOperationKey => $objectOperation) {
            if ($objectOperation === 'u') {
                $objectUpdate = $this->objectsInserts[$objectOperationKey];

                $LJReflection->setObject($objectUpdate);
                $objectUpdateByArray = $LJReflection->extractToArray();

                $doBind = array_intersect_ukey($objectUpdateByArray,
                    $this->mappers[$this->objectsMappedClass[$objectOperationKey]]->getColumnsNameIndexedByPropertyName(),
                    function($firstKey, $secondKey)
                    {
                        return $firstKey <=> $secondKey;
                    }
                );

                if ($this->j[$objectOperationKey] !== json_encode($doBind)) {
                    $this->j[$objectOperationKey] = json_encode($doBind);

                    $doPrepareStatementUpdate = $this->connection->prepare($this->prepareStatementsUpdates[$this->objectHashByPrepareStatementsUpdates[$objectOperationKey]]);

                    foreach($doBind as $doBindKey => $doBindValue) {
                        $bindColumnName = $this->mappers[$this->objectsMappedClass[$objectOperationKey]]->getColumnNameByPropertyName($doBindKey);
                        $doPrepareStatementUpdate->bindValue($bindColumnName, $doBindValue);
                    }

                    $doPrepareStatementUpdate->execute();
                }
            }

            $this->objectsOperations[$objectOperationKey] = 'u';
        }
    }

    public function saveChanges(): bool
    {
        $this->connectDatabase();
        $this->connection->beginTransaction();
        $this->persistInserts();
        $this->persistUpdates();
        $this->connection->commit();

        return true;
    }
}
