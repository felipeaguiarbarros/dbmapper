<?php

namespace Nwz\DBMapper\Mapper\Constraint\PrimaryKey;

use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategyEmptyStrategyTypeException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategyInvalidTypeException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategySequenceNameMustNotBeInformedException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\Exception\PrimaryKeyStrategySequenceNameMustBeInformedException;

class PrimaryKeyStrategy implements PrimaryKeyStrategyInterface
{
    private const PRIMARY_KEY_STRATEGY_TYPES = [
        self::NONE => self::NONE,
        self::SEQUENCE => self::SEQUENCE
    ];

    private $primaryKeyStrategyType = '';
    private $primaryKeySequenceName = '';

    public function __construct(string $primaryKeyStrategyType, string $sequenceNameWhenStrategyTypeIsSequence = '')
    {
        $_primaryKeyStrategyType = trim($primaryKeyStrategyType);
        $_sequenceNameWhenStrategyTypeIsSequence = trim($sequenceNameWhenStrategyTypeIsSequence);

        if ($_primaryKeyStrategyType === '') {
            throw new PrimaryKeyStrategyEmptyStrategyTypeException();
        }

        if (($_primaryKeyStrategyType !== '') &&
            (!isset(self::PRIMARY_KEY_STRATEGY_TYPES[$_primaryKeyStrategyType]))) {
            throw new PrimaryKeyStrategyInvalidTypeException();
        }

        if (($_primaryKeyStrategyType !== self::SEQUENCE) &&
            ($_sequenceNameWhenStrategyTypeIsSequence !== '')) {
            throw new PrimaryKeyStrategySequenceNameMustNotBeInformedException();
        }

        if (($_primaryKeyStrategyType === self::SEQUENCE) &&
            ($_sequenceNameWhenStrategyTypeIsSequence === '')) {
            throw new PrimaryKeyStrategySequenceNameMustBeInformedException();
        }

        $this->primaryKeyStrategyType = $_primaryKeyStrategyType;
        $this->primaryKeySequenceName = $_sequenceNameWhenStrategyTypeIsSequence;
    }

    public function getPrimaryKeyStrategyType(): string
    {
        return $this->primaryKeyStrategyType;
    }

    public function getPrimaryKeySequenceName(): string
    {
        return $this->primaryKeySequenceName;
    }
}
