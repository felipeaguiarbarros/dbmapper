<?php

namespace Nwz\DBMapper\Mapper\Constraint\PrimaryKey;

interface PrimaryKeyStrategyInterface
{
    const NONE = 'none';
    const SEQUENCE = 'sequence';
}
