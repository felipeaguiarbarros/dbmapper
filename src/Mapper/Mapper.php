<?php

namespace Nwz\DBMapper\Mapper;

use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategy;
use Nwz\DBMapper\Mapper\Exception\MappedClassIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\TableNameIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\TableHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\PropertyNameIsNotDefinedException;
use Nwz\DBMapper\Mapper\Exception\PropertyHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\ColumnHasAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Exception\PropertyHasNotAlreadyBeenMappedException;
use Nwz\DBMapper\Mapper\Constraint\PrimaryKey\PrimaryKeyStrategyInterface;

abstract class Mapper implements MapperInterface
{
    private $mappedClass = '';
    private $tableName = '';
    private $propertiesNameIndexedByColumnName = [];
    private $columnsNameIndexedByPropertyName = [];
    private $primaryKeyPropertyName = '';
    private $primaryKeyColumnName = '';
    private $primaryKeyStrategy = null;

    public function setMappedClass(string $mappedClass): void
    {
        if (trim($mappedClass) === '') {
            throw new MappedClassIsNotDefinedException();
        }

        $this->mappedClass = $mappedClass;
    }

    public function getMappedClass(): string
    {
        return $this->mappedClass;
    }

    public function setTableName(string $tableName): void
    {
        if (trim($tableName) === '') {
            throw new TableNameIsNotDefinedException();
        }

        if (trim($this->tableName) !== '') {
            throw new TableHasAlreadyBeenMappedException();
        }

        $this->tableName = trim($tableName);
    }

    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function setColumn(string $propertyName, string $columnName = ''): void
    {
        if (trim($propertyName) === '') {
            throw new PropertyNameIsNotDefinedException();
        }

        trim($columnName) !== '' ?: $columnName = $propertyName;

        if (isset($this->columnsNameIndexedByPropertyName[$propertyName])) {
            throw new PropertyHasAlreadyBeenMappedException();
        }

        if (isset($this->propertiesNameIndexedByColumnName[$columnName])) {
            throw new ColumnHasAlreadyBeenMappedException();
        }

        $this->propertiesNameIndexedByColumnName[$columnName] = $propertyName;
        $this->columnsNameIndexedByPropertyName[$propertyName] = $columnName;
    }

    public function getPropertiesNameIndexedByColumnName(): array
    {
        return $this->propertiesNameIndexedByColumnName;
    }

    public function getColumnsNameIndexedByPropertyName(): array
    {
        return $this->columnsNameIndexedByPropertyName;
    }

    public function getColumnNameByPropertyName(string $propertyName): string
    {
        return $this->columnsNameIndexedByPropertyName[$propertyName];
    }

    public function setPrimaryKey(string $propertyName, PrimaryKeyStrategyInterface $primaryKeyStrategy): void
    {
        if (trim($propertyName) === '') {
            throw new PropertyNameIsNotDefinedException();
        }

        if (!isset($this->columnsNameIndexedByPropertyName[$propertyName])) {
            throw new PropertyHasNotAlreadyBeenMappedException();
        }

        $this->primaryKeyPropertyName = $propertyName;
        $this->primaryKeyColumnName = $this->columnsNameIndexedByPropertyName[$propertyName];
        $this->primaryKeyStrategy = $primaryKeyStrategy;
    }

    public function getPrimaryKeyPropertyName(): string
    {
        return $this->primaryKeyPropertyName;
    }

    public function getPrimaryKeyColumnName(): string
    {
        return $this->primaryKeyColumnName;
    }

    public function getPrimaryKeyStrategy(): PrimaryKeyStrategy
    {
        return $this->primaryKeyStrategy;
    }
}
