<?php

namespace Nwz\DBMapper\LJReflection;

use Nwz\DBMapper\LJReflection\Exceptions\ObjectIsNotSettingException;

class LJReflection
{
    private $object;

    public function setObject(Object $object): void
    {
        $this->object = $object;
    }

    public function getObject(): Object
    {
        return $this->object;
    }

    public function extractToArray(): array
    {
        if ($this->object === null) {
            throw new ObjectIsNotSettingException();
        }

        $reflectionObject = new \ReflectionObject($this->object);
        $array = [];

        foreach ($reflectionObject->getProperties() as $reflectionObjectProperty) {
            $reflectionObjectProperty->setAccessible(true);
            $value = $reflectionObjectProperty->getValue($this->object);
            $array[$reflectionObjectProperty->getName()] = $value;
        }

        return $array;
    }

    public function setPropertyValue(string $propertyName, $propertyValue)
    {
        $reader = function & ($object, $propertyName) {
            $propertyValue = &\Closure::bind(function & () use ($propertyName) {
                return $this->$propertyName;
                },
                $object,
                $object
            )->__invoke();

            return $propertyValue;
        };

        $result = &$reader($this->object, $propertyName);
        $result = $propertyValue;
    }
}
